﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enhanced_online_shoppping
{
    public class MenuDisplay
    {
        public void MainMenu()
        {
            Console.WriteLine("==========MENU==========");
            Console.WriteLine("| 1. View products     |");
            Console.WriteLine("| 2. Add to cart       |");
            Console.WriteLine("| 3. Remove from cart  |");
            Console.WriteLine("| 4. View cart         |");
            Console.WriteLine("| 5. Checkout          |");
            Console.WriteLine("| 6. Exit              |");
            Console.WriteLine("========================");
            Console.Write("Enter your choice: ");
        }
    }
}
