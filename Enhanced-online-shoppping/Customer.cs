﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enhanced_online_shoppping
{
    public class Customer
    {
        public int CustomerID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public List<Product> ShoppingCart { get; } = new List<Product>();

        public void AddToCart(Product product, int quantity = 1)
        {
            if(quantity > product.StockQuantity)
            {
                throw new Exception("Your quantity can not bigger than stock quantity");
            }
            if(ShoppingCart.Contains(product))
            {
                product.InventoryQuantity += quantity;
            }
            else
            {
                product.InventoryQuantity += quantity;
                ShoppingCart.Add(product);
            }
            product.StockQuantity -= 1;
        }
        public void RemoveFromCart(Product product)
        {      
            if (ShoppingCart.Remove(product))
            {
                product.StockQuantity += product.InventoryQuantity;
                product.InventoryQuantity = 0;
            }
            else
            {
                throw new Exception("Remove fail");
            }
        }

        public string ViewCart()
        {
            StringBuilder sb = new StringBuilder();
            foreach (Product product in ShoppingCart)
            {
                sb.Append($"\n{product.Name} - {product.Price} - {product.InventoryQuantity}");
            }
            return sb.ToString();
        }

        public decimal Checkout()
        {
            decimal totalPrice = 0;
            decimal totalEachProduct = 1;
            foreach (Product product in ShoppingCart)
            {
                totalEachProduct = product.InventoryQuantity * product.Price;
                totalPrice += totalEachProduct;    
            }
            ShoppingCart.Clear();
            return totalPrice;
        }
    }
}
