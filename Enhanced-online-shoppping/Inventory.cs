﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enhanced_online_shoppping
{
    public class Inventory
    {
        public List<Product> AvailableProduct { get; } = new List<Product>();
        public void AddProduct(Product product)
        {
            AvailableProduct.Add(product);
        }

        public void RemoveProduct(Product product)
        {
            AvailableProduct.Remove(product);
        }

        public void UpdateStock (Product product, int quantity)
        {
            product.StockQuantity += quantity;
        }

        public bool CheckStock(Product product, int quantity)
        {
            return product.StockQuantity >= quantity;
        }
        public string ViewAllAvailableProduct()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in AvailableProduct)
            {
                sb.Append($"\n{item.ProductID} - {item.Name} - {item.Price} - {item.StockQuantity}");
            }
            return sb.ToString();
        }
    }
}
