﻿using Enhanced_online_shoppping;

MenuDisplay menu = new MenuDisplay();
Inventory inventory = new Inventory();
Customer customer = new Customer();
inventory.AddProduct(new Product { ProductID = 1, Name = "Product A", Price = 1099, StockQuantity = 10 });
inventory.AddProduct(new Product { ProductID = 2, Name = "Product B", Price = 1599, StockQuantity = 8 });
inventory.AddProduct(new Product { ProductID = 3, Name = "Product C", Price = 599, StockQuantity = 15 });
int choice = 0;
do
{
	try
	{
		menu.MainMenu();
		choice = int.Parse(Console.ReadLine());
		switch (choice)
		{
			case 1:
                Console.WriteLine(inventory.ViewAllAvailableProduct());
				break;
			case 2:
                Console.Write("Enter product id: ");
				int productID = int.Parse(Console.ReadLine());
				var product = inventory.AvailableProduct.Find(p => p.ProductID == productID);
				if(product is not null)
				{
					customer.AddToCart(product);
                }
                break;
			case 3:
                Console.WriteLine("Enter product id: ");
				int removeID = int.Parse(Console.ReadLine());
				var removeProduct = inventory.AvailableProduct.Find(p => p.ProductID == removeID);
				if(removeProduct is not null)
				{
					customer.RemoveFromCart(removeProduct);
				}
                break;
			case 4:
                Console.WriteLine(customer.ViewCart());
                break;
			case 5:
				var total = customer.Checkout();
                Console.WriteLine(total);
                break;
			case 6:
                Console.WriteLine("Bye bye");
				Environment.Exit(0);
                break;
			default:
				throw new Exception("Please enter in the range 1-6");
		}
	}
	catch (Exception ex)
	{
        Console.WriteLine(ex.Message);
    }
} while (choice !=6);